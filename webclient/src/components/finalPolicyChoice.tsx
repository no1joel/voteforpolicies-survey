import React, { useState } from 'react'
import * as ApiTypes from 'vfp-survey-lib/types'
import classes from './finalPolicyChoice.module.scss'
import SurveyButton from './surveyButton'

type Props = {
  maybeIndexes: number[]
  onPicked: (policyIndex: number) => void
  policyIndexeCrossRef: number[]
  policyList: ApiTypes.PartyPolicy[]
  partyLabel: string
}

const FinalPolicyChoice = ({
  maybeIndexes,
  onPicked,
  policyIndexeCrossRef,
  policyList,
  partyLabel,
}: Props) => {
  const [choice, setChoice] = useState<number | null>(
    maybeIndexes.length === 1 ? maybeIndexes[0] : null
  )

  const test = (index: number) => {
    if (index === choice) {
      onPicked(policyIndexeCrossRef[index])
    } else {
      setChoice(index)
    }
  }

  const displayPolicies = (index: number, selected: boolean) => {
    const partyNum = index + 1
    const policyIndex = policyIndexeCrossRef[index]
    const buttonLabel = selected ? 'Go to next step' : 'Select these policies'
    return (
      <div key={index} className={classes.policySet}>
        <div className={classes.constrain}>
          <h2 className={classes.policySetTitle}>
            {partyLabel} {partyNum}
          </h2>
          <ul className={classes.policySetPolicies}>
            {policyList[policyIndex].policies.map((p, k) => (
              <li key={k}>{p}</li>
            ))}
          </ul>
          <div className={classes.selectPolicy}>
            <div className={classes.selectPolicyButtons}>
              <div className={classes.buttonGrid}>
                <SurveyButton
                  onClick={() => test(index)}
                  buttonLabel={buttonLabel}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }

  return <div>{maybeIndexes.map(i => displayPolicies(i, choice === i))}</div>
}

export default FinalPolicyChoice
