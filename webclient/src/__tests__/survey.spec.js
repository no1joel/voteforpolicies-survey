import React from 'react'
import {
  fireEvent,
  render,
  screen,
  waitForElementToBeRemoved,
} from '@testing-library/react'
import Survey from '../components/survey'
import * as surveyApi from '../api/survey'
import text from '../translations/en'

describe('<Survey /> initial states before interaction', () => {
  it('displays loading text, calls api and displays issues', async () => {
    const spy = jest.spyOn(surveyApi, 'getCurrent')

    render(<Survey />)

    screen.getByText('Loading...')

    expect(spy).toHaveBeenCalled()
    await waitForElementToBeRemoved(() => screen.getByText('Loading...'))

    // Render result
    screen.findByText(text.selectAll)
    const { issues } = global.surveyExampleData
    for (let i = 0; i < issues.length - 1; i++) {
      screen.getByText(issues[i].name)
    }

    // Select all issues
    const allCheckboxes = screen.getAllByRole('checkbox')
    allCheckboxes.forEach(c => expect(c).not.toBeChecked())
    fireEvent.click(allCheckboxes[0])
    allCheckboxes.forEach(c => expect(c).toBeChecked())
  })
})
