import client from './client'

import * as ApiTypes from 'vfp-survey-lib/types'

export const getCurrent = () => client.get('/survey/current')

export function submit (
  selections: ApiTypes.Selection[],
  filters: ApiTypes.Filter[]
) {
  return client.post('/survey/submit', { selections, filters })
}
