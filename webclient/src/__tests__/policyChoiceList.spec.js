import { render, screen } from '@testing-library/react'
import React from 'react'
import PolicyChoiceList from '../components/policyChoiceList'

describe('<PolicyChoiceList />', () => {
  it('displays correct policy choices', () => {
    const issues = [
      { id: 1, name: 'Crime' },
      { id: 2, name: 'Democracy' },
      { id: 3, name: 'Economy' },
    ]
    const parties = [
      { id: 1, name: 'Lib' },
      { id: 2, name: 'Lab' },
      { id: 3, name: 'Con' },
    ]
    const policySelections = [
      { issueId: 2, partyId: 3 },
      { issueId: 3, partyId: 1 },
      { issueId: 1, partyId: 2 },
    ]

    render(
      <PolicyChoiceList
        issues={issues}
        parties={parties}
        policySelections={policySelections}
      />
    )

    const getPartyBlockByContent = text =>
      screen.getByText(
        (_content, element) =>
          element.classList.contains('partyBlock') &&
          element.textContent === text
      )
    getPartyBlockByContent('DemocracyCon')
    getPartyBlockByContent('EconomyLib')
    getPartyBlockByContent('CrimeLab')
  })
})
