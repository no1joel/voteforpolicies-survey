import { fireEvent, render, screen } from '@testing-library/react'
import React from 'react'
import SurveyButton from '../components/surveyButton'

describe('<SurveyButton />', () => {
  it('displays the button text', () => {
    render(<SurveyButton buttonLabel='Hello world!' />)
    expect(screen.getByRole('button')).toHaveTextContent('Hello world!')
  })

  it('calls the click handler when the button is clicked', () => {
    const clickHandler = jest.fn()
    render(<SurveyButton buttonLabel='Hello world!' onClick={clickHandler} />)
    fireEvent.click(screen.getByRole('button'))
    expect(clickHandler.mock.calls.length).toEqual(1)
  })

  it('includes class name when given as prop', () => {
    const { container } = render(<SurveyButton className={'my-button-class'} />)
    expect(container.firstChild.classList).toContain('my-button-class')
  })
})
