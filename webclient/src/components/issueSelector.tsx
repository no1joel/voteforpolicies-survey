import React, { useState } from 'react'
import Issue from './issue'
import SurveyButton from './surveyButton'
import IssueSelectionStatus from './issueSelectionStatus'
import classes from './issueSelector.module.scss'
import t from '../translations'

import * as ApiTypes from 'vfp-survey-lib/types'

interface IssueWithSelectType extends ApiTypes.Issue {
  selected: boolean
}

type Props = {
  issueList: ApiTypes.Issue[]
  onComplete: (issueIds: number[]) => void
}

const IssueSelector = ({ issueList, onComplete }: Props) => {
  const [allSelected, setAllSelected] = useState(false)
  const [issues, setIssues] = useState<IssueWithSelectType[]>(
    issueList.map(c => {
      return { selected: false, ...c }
    })
  )

  const selectedCount = () => issues.filter(c => c.selected).length

  const handleChange = (issueId: number | null) => {
    if (issueId === null) {
      setAllSelected(!allSelected)
      setIssues(
        issues.map(c => {
          return { ...c, selected: !allSelected }
        })
      )
    } else {
      const index = issues.findIndex(c => c.id === issueId)
      setIssues(
        issues.map((i, idx) =>
          idx !== index ? i : { ...i, selected: !i.selected }
        )
      )
    }
  }

  const handleButtonClick = () => {
    if (selectedCount()) {
      onComplete(issues.filter(c => c.selected).map(c => c.id))
    }
  }

  return (
    <div className={classes.layout}>
      <div className={classes.layoutInner}>
        <div className={classes.issues}>
          <fieldset>
            <legend className={classes.legend}>
              <span className={classes.header}>
                <span className={classes.headerH2}>{t('issuesHeading')}</span>
              </span>
            </legend>
            <ul className={classes.issueSelector}>
              <Issue
                key='all'
                issue={{
                  id: null,
                  name: t('selectAll'),
                  selected: allSelected,
                }}
                onChange={handleChange}
              />
              {issues.map(c => (
                <Issue key={c.id} issue={c} onChange={handleChange} />
              ))}
            </ul>
            <IssueSelectionStatus numSelected={selectedCount()} />
            <div className={classes.startBtn}>
              <SurveyButton
                onClick={handleButtonClick}
                buttonLabel={t('startSurvey')}
              />
            </div>
          </fieldset>
        </div>
      </div>
    </div>
  )
}

export default IssueSelector
