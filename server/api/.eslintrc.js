module.exports = {
  env: {
    browser: true,
    es6: true,
    'jest/globals': true
  },
  extends: [
    'standard'
  ],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly'
  },
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaFeatures: {
      jsx: true
    },
    ecmaVersion: 2018,
    sourceType: 'module'
  },
  plugins: [
    'jest',
    '@typescript-eslint'
  ],
  rules: {
    "comma-dangle": 0,
    "no-unused-vars": "off",
    "no-use-before-define": "off",
    "@typescript-eslint/no-unused-vars": "error",
  }
}
