import React from 'react'
import { render, screen } from '@testing-library/react'
import IssueSelectionStatus from '../components/issueSelectionStatus'
import t from '../translations'

describe('<IssueSelectionStatus />', () => {
  it('displays selectAtLeastOneIssue msg', () => {
    render(<IssueSelectionStatus numSelected={0} />)
    screen.getByText(t('selectAtLeastOneIssue'))
  })

  it('displays correct selection statuses', () => {
    const { rerender } = render(<IssueSelectionStatus numSelected={1} />)
    screen.getByText(
      t('surveyTimeEstimateTemplate', { count: 1, from: 2, to: 3 })
    )

    rerender(<IssueSelectionStatus numSelected={937} />)
    screen.getByText(
      t('surveyTimeEstimateTemplate', { count: 937, from: 1874, to: 2811 })
    )
  })
})
