import React from 'react'
import { render, screen } from '@testing-library/react'
import ResultsDisplay from '../components/resultsDisplay'

describe('<ResultsDisplay />', () => {
  it('renders title', () => {
    const title = 'Results title'
    render(<ResultsDisplay title={title} results={{ total: 1, byParty: [] }} />)
    screen.getByText(title)
  })

  it('render an SVG', () => {
    const { container } = render(
      <ResultsDisplay title='page title' results={{ total: 1, byParty: [] }} />
    )

    expect(container.querySelector('svg')).not.toBeNull()
  })
})
