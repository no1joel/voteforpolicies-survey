import * as ApiTypes from 'vfp-survey-lib/types'

export interface Cache {
  init: () => void
  getSurveyData: (surveyId: number) => any
  getCandidateList: (surveyId: number) => ApiTypes.Party[]
  getConstituencyData: (surveyId: number, constituencyId: number)
      => ApiTypes.Constituency
  getDivisionResults: (surveyId: number, divisionLvl: number, divisionId: number)
      => ApiTypes.Result
  getLookup: (surveyId: number, lookupRef: string) => number | null
}
