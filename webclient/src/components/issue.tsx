import React from 'react'
import classes from './issue.module.scss'

type Props = {
  issue: { id: number | null; name: string; selected: boolean }
  onChange: (issueId: number | null) => void
}

const Issue = ({ issue, onChange }: Props) => {
  const id = issue.id?.toString() || 'all'

  return (
    <li className={classes.issue}>
      <div className={classes.formIcon}>
        <input
          checked={issue.selected}
          className={classes.formIconInput}
          id={id}
          onChange={() => onChange(issue.id)}
          type='checkbox'
        />
        <label className={classes.issueLabel} htmlFor={id}>
          {issue.name}
        </label>
      </div>
    </li>
  )
}

export default Issue
