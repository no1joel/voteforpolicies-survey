import React from 'react'
import { ResultsFormattedType } from '../interfaces'
import { PieChart, Pie, Cell, ResponsiveContainer } from 'recharts'
const { contrastColor } = require('contrast-color')

type Props = {
  results: ResultsFormattedType
}

const partyColours: { [key: string]: string } = {
  'Brexit Party': '#5CBDD3',
  Conservatives: '#0087DC',
  'Liberal Democrats': '#FDBB30',
  Labour: '#D50000',
  'Green Party': '#75A92D',
  'Plaid Cymru': '#3E8424',
  'Alliance Party of Northern Ireland': '#F8CE00',
  'Social Democratic and Labour Party': '#006E51',
  'Sinn Féin': '#006C2A',
  'Democratic Unionist Party': '#CE0000',
  'Scottish National Party': '#000',
  'Scottish Greens': '#00B543'
}

const convertToData = (byPartyArray: ResultsFormattedType['byParty']) => {
  return byPartyArray.map((byParty: { name: string; count: number }) => {
    return { name: byParty.name, value: byParty.count }
  })
}

const createCells = (byPartyArray: ResultsFormattedType['byParty']) => {
  return byPartyArray.map((byParty: { name: string; count: number }, index) => {
    return <Cell key={`cell-${index}`} fill={partyColours[byParty.name]} />
  })
}

const RADIAN = Math.PI / 180
const renderCustomizedLabel = ({
  cx,
  cy,
  midAngle,
  innerRadius,
  outerRadius,
  percent,
  fill
}: any) => {
  let labelHeight
  if (percent <= 0.05) {
    return null
  } else if (percent <= 0.1) {
    labelHeight = 0.65
  } else if (percent <= 0.3) {
    labelHeight = 0.6
  } else if (percent <= 0.6) {
    labelHeight = 0.5
  } else {
    labelHeight = 0.45
  }

  const radius = innerRadius + (outerRadius - innerRadius) * labelHeight
  let x = cx + radius * Math.cos(-midAngle * RADIAN)
  let y = cy + radius * Math.sin(-midAngle * RADIAN)

  if (percent === 1) {
    x = cx
    y = cy
  }

  return (
    <text
      x={x}
      y={y}
      fill={contrastColor({ bgColor: fill, threshold: 150 })}
      textAnchor='middle'
      dominantBaseline='central'
      fontFamily="lato, arial, sans-serif"
    >
      {`${(percent * 100).toFixed(1)}%`}
    </text>
  )
}

const ResultsPie = ({ results }: Props) =>
  <ResponsiveContainer minHeight={500}>
    <PieChart>
      <Pie
        data={convertToData(results.byParty)}
        cx='50%'
        cy='50%'
        startAngle={-270}
        labelLine={false}
        label={renderCustomizedLabel}
        dataKey='value'
        isAnimationActive={false}
        stroke='0px'
      >
        {createCells(results.byParty)}
      </Pie>
    </PieChart>
  </ResponsiveContainer>

export default ResultsPie
