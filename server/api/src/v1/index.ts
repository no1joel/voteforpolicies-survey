import express, * as Express from 'express'
import { Cache } from '../cache/types'
import * as ApiTypes from 'vfp-survey-lib/types'

const app: Express.Application = express()
app.use(express.json())

let cache: Cache

export function init (cacheModule: Cache) {
  cache = cacheModule
  cache.init()
}

app.get('/survey/current', (req: Express.Request, res: Express.Response) => {
  res.send(cache.getSurveyData(0))
})

app.post('/survey/submit', (req: Express.Request, res: Express.Response) => {
  let postcode: string
  try {
    postcode = String(req.body.filters[0].value)
  } catch (err) {
    console.log('Postcode filter missing from body: ', req.body)
    postcode = 'NOTFOUND'
  }

  const constituency = cache.getLookup(0, postcode)
  let constituencyData: ApiTypes.Constituency
  let constituencyResults: ApiTypes.Result
  let countryResults: ApiTypes.Result

  if (constituency === null) {
    constituencyData = {
      id: -1,
      name: 'Postcode not found',
      countryId: -1,
    }
    constituencyResults = {
      surveyCount: 0,
      results: [],
    }
    countryResults = constituencyResults
  } else {
    constituencyData = cache.getConstituencyData(0, constituency)
    constituencyResults = cache.getDivisionResults(0, 2, constituency)
    countryResults = cache.getDivisionResults(0, 1, constituencyData.countryId)
  }

  const response = {
    resultsId: 'xEngGxGC4GgYf3u7',
    parties: cache.getCandidateList(0),
    selections: [],
    constituency: constituencyData,
    results: {
      constituency: constituencyResults,
      country: countryResults,
      nation: cache.getDivisionResults(0, 0, 0)
    }
  }

  res.send(response)
})

export default app
