describe('My First Test', () => {
  it('Does not do much!', () => {
    expect(true).to.equal(true)
  })
})

describe('My Second Test', () => {
  it('Visits the homepage', () => {
    cy.visit('/')
    cy.contains('Vote, for policies!')
  })
})
