import * as functions from 'firebase-functions'
import express from 'express'

import appV1, * as AppV1 from './api/v1'
import * as Cache from './api/cache/file'

const funcConfig: functions.config.Config = functions.config()
const corsOrigin: string = (funcConfig.api || {}).cors || ''

const webApi: express.Express = express()

if (corsOrigin) {
  webApi.use(function (req: express.Request, res: express.Response, next) {
    res.header('Access-Control-Allow-Origin', corsOrigin)
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
    next()
  })
}

AppV1.init(Cache)

webApi.use('/api/v1', appV1)
export const apiFunc: functions.HttpsFunction = functions.https.onRequest(webApi)
