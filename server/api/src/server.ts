import express, * as Express from 'express'
import appV1, * as AppV1 from './v1'

import * as Cache from './cache/file'

require('dotenv').config()

const port: string = process.env.PORT || '5000'
const protocol: string = process.env.PROTOCOL || 'http'
const host: string = process.env.HOST || 'localhost'
const corsOrigin: string = process.env.CORS_ORIGIN || ''

const app: Express.Application = express()

AppV1.init(Cache)

if (corsOrigin) {
  app.use(function (req: Express.Request, res: Express.Response, next) {
    res.header('Access-Control-Allow-Origin', corsOrigin)
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
    next()
  })
}

app.use('/api/v1', appV1)

// Route not found (404)
app.use(function (req: Express.Request, res: Express.Response) {
  return res.status(404).send()
})

app.listen(port, () => {
  console.log(`Listening at ${protocol}://${host}:${port}`)
})
