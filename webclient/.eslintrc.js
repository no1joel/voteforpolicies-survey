module.exports = {
  env: {
    browser: true,
    es6: true,
    "jest/globals": true
  },
  extends: [
    'plugin:react/recommended',
    'standard'
  ],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly'
  },
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaFeatures: {
      jsx: true
    },
    ecmaVersion: 2018,
    sourceType: 'module'
  },
  plugins: [
    'jest',
    'react',
    '@typescript-eslint'
  ],
  rules: {
    "comma-dangle": 0,
    "no-unused-vars": "off",
    "no-use-before-define": "off",
    "@typescript-eslint/no-unused-vars": "error",
    "react/prop-types": "off"
  },
  settings: {
    "react": {
      "version": "detect"
    }
  }
}
