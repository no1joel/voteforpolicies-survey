/*
  File Cache

  A data cache based on files.

  It is only meant as a baby step to a real cache system. In cloud hosting
  we don't intend to store or amend files therefore it is a read only system.
*/

import * as ApiTypes from 'vfp-survey-lib/types'

let store: {survey: any}

export function init () {
  const current = require('./data/current.json')
  const lookup = require('./data/lookup.json')
  const results = require('./data/results.json')

  store = {
    survey: {
      0: {
        data: current,
        candidates: results.parties,
        lookup: lookup.lookup,
        constituencies: results.constituencies,
        results: results.results,
      }
    }
  }
}

function get (surveyId: number, lookups: string[]):
    any | ApiTypes.Party[] | ApiTypes.Result | ApiTypes.Constituency | ApiTypes.Country | number {
  let ptr = store.survey[surveyId]
  if (typeof ptr === 'undefined') {
    throw new Error(`Invalid survey id: ${surveyId}`)
  }

  let level = 0
  lookups.forEach(l => {
    ptr = ptr[l]
    level++
    if (typeof ptr === 'undefined') {
      throw new Error(`Cannot find ${l} at ${level} level`)
    }
  })

  return ptr
}

export function getSurveyData (surveyId: number): any {
  return get(surveyId, ['data'])
}

export function getCandidateList (surveyId: number): ApiTypes.Party[] {
  return get(surveyId, ['candidates'])
}

export function getDivisionResults (surveyId: number, divisionLvl: number, divisionId: number): ApiTypes.Result {
  try {
    return get(surveyId, ['results', divisionLvl.toString(), divisionId.toString()])
  } catch (err) {
    if (err instanceof Error) {
      if ((err as Error).message.endsWith('3 level')) {
        throw new Error('Invalid division')
      } else if (err.message.endsWith('2 level')) {
        throw new Error('Invalid division level')
      }
    }
    throw (err)
  }
}

export function getConstituencyData (surveyId: number, constituencyId: number): ApiTypes.Constituency {
  try {
    return get(surveyId, ['constituencies', constituencyId.toString()])
  } catch (err) {
    if (err instanceof Error && err.message.endsWith('2 level')) {
      throw new Error('Invalid constituency id')
    } else {
      throw (err)
    }
  }
}

export function getLookup (surveyId: number, lookupRef: string): number | null {
  try {
    return get(surveyId, ['lookup', lookupRef])
  } catch (err) {
    if (err instanceof Error && err.message.startsWith('Cannot find')) {
      return null
    } else {
      throw (err)
    }
  }
}
