import React, { useState } from 'react'
import PolicyChoiceList from './policyChoiceList'
import ResultsDisplay from './resultsDisplay'
import t from '../translations'
import * as ApiTypes from 'vfp-survey-lib/types'
import { ResultsFormattedType } from '../interfaces'
import classes from './surveyResults.module.scss'

type Props = {
  country: ApiTypes.Country
  constituency: ApiTypes.Constituency
  issues: ApiTypes.Issue[]
  parties: ApiTypes.Party[]
  selections: ApiTypes.Selection[]
  results: {
    constituency: ApiTypes.Result
    country: ApiTypes.Result
    nation: ApiTypes.Result
  }
}

const tabList = {
  PERSONAL_RESULTS: 1,
  CONSTITUENCY_RESULTS: 2,
  COUNTRY_RESULTS: 3,
  TOTAL_RESULTS: 4,
}

const SurveyResults = ({
  country,
  constituency,
  issues,
  parties,
  selections,
  results,
}: Props) => {
  const [tab, setTab] = useState<number>(tabList.PERSONAL_RESULTS)

  const resultsFromSelection = (
    selections: ApiTypes.Selection[]
  ): ResultsFormattedType => {
    const counts: any = {}

    selections.forEach(s => {
      counts[s.partyId] = counts[s.partyId] ? counts[s.partyId] + 1 : 1
    })

    const byParty: { name: string; count: number }[] = []

    for (const key of Object.keys(counts)) {
      const party = parties.find(p => p.id === Number(key))
      byParty.push({ name: String(party?.name) || 'Err', count: counts[key] })
    }

    return {
      total: selections.length,
      byParty: byParty.sort((a, b) => (a.count < b.count ? 1 : -1)),
    }
  }

  const resultsFromTotals = (
    results: { partyId: number; count: number }[]
  ): ResultsFormattedType => {
    let sum: number = 0

    const byParty: { name: string; count: number }[] = []

    for (const key of Object.keys(results)) {
      sum = sum + results[Number(key)].count
      const party = parties.find(p => p.id === results[Number(key)].partyId)
      byParty.push({
        name: String(party?.name) || 'Err',
        count: results[Number(key)].count,
      })
    }

    return {
      total: sum,
      byParty: byParty.sort((a, b) => (a.count < b.count ? 1 : -1)),
    }
  }

  const onClick = (newTab: number) => setTab(newTab)

  const tabbedState = () => {
    switch (tab) {
      case tabList.PERSONAL_RESULTS: {
        return (
          <div>
            <ResultsDisplay
              title={t('results.selectedPartiesHdr')}
              results={resultsFromSelection(selections)}
            />
            <p />
            <PolicyChoiceList
              issues={issues}
              parties={parties}
              policySelections={selections}
            />
          </div>
        )
      }
      case tabList.CONSTITUENCY_RESULTS: {
        return (
          <ResultsDisplay
            title={t('results.resultsHdr') + ': ' + constituency.name}
            results={resultsFromTotals(results.constituency.results)}
          />
        )
      }
      case tabList.COUNTRY_RESULTS: {
        return (
          <ResultsDisplay
            title={t('results.overallResultsHdr') + ' ' + country.name}
            results={resultsFromTotals(results.country.results)}
          />
        )
      }
      case tabList.TOTAL_RESULTS: {
        return (
          <ResultsDisplay
            title={t('results.overallResultsHdr') + ' the United Kingdom'}
            results={resultsFromTotals(results.nation.results)}
          />
        )
      }
    }
  }

  return (
    <>
      <div className={classes.layout}>
        <div className={classes.headerContainer}>
          <h2 className={classes.header}>{t('results.resultsIntro')}</h2>
        </div>
      </div>
      <div className={classes.resultsTabs}>
        <div className={classes.tabbing}>
          <div className={classes.constrain}>
            <ul className={classes.tabList}>
              <li className={classes.tabListItem}>
                <a
                  aria-selected={tab === tabList.PERSONAL_RESULTS}
                  className={classes.tabLink}
                  href='#personal-results'
                  onClick={() => {
                    onClick(tabList.PERSONAL_RESULTS)
                  }}
                >
                  Personal results
                </a>
              </li>
              <li className={classes.tabListItem}>
                <a
                  aria-selected={tab === tabList.CONSTITUENCY_RESULTS}
                  className={classes.tabLink}
                  href='#constituency-results'
                  onClick={() => {
                    onClick(tabList.CONSTITUENCY_RESULTS)
                  }}
                >
                  Constituency results
                </a>
              </li>
              <li className={classes.tabListItem}>
                <a
                  aria-selected={tab === tabList.COUNTRY_RESULTS}
                  className={classes.tabLink}
                  href='#country-results'
                  onClick={() => {
                    onClick(tabList.COUNTRY_RESULTS)
                  }}
                >
                  {country.name} results
                </a>
              </li>
              <li className={classes.tabListItem}>
                <a
                  aria-selected={tab === tabList.TOTAL_RESULTS}
                  className={classes.tabLink}
                  href='#total-results'
                  onClick={() => {
                    onClick(tabList.TOTAL_RESULTS)
                  }}
                >
                  National results
                </a>
              </li>
            </ul>
          </div>
        </div>
        {tabbedState()}
      </div>
    </>
  )
}

export default SurveyResults
