import React from 'react'
import classes from './issueSelectionStatus.module.scss'
import t from '../translations'

type Props = {
  numSelected: number
}

const IssueSelectionStatus = ({ numSelected }: Props) => {
  const getDisplayMessage = () => {
    if (numSelected > 0) {
      return t('surveyTimeEstimateTemplate', {
        count: numSelected,
        from: numSelected * 2,
        to: numSelected * 3,
      })
    } else {
      return t('selectAtLeastOneIssue')
    }
  }

  return (
    <div className={classes.center}>
      <span className={classes.selectionStatus}>
        <i className={classes.icon} />
        {getDisplayMessage()}
      </span>
    </div>
  )
}

export default IssueSelectionStatus
