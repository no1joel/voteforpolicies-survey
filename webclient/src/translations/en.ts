const Language = {
  issue: 'Issue',
  issuesHeading: 'Which issues are important to you?',
  maybe: 'Maybe',
  next: 'Next',
  noThanks: 'No thanks',
  party: 'Party',
  showRelevantPolicies: 'To show you relevant policies…',
  whichCountry: 'From which country will you be voting?',
  nIrelandPolicies: 'Sorry, N.Ireland policies are ',
  notAvailable: 'not available.',
  postcodeEnter: 'Enter your postcode',
  postcodeEntryLabel: 'Find out which parties are popular in your constituency',
  policySelectIntro:
    "Here are the key {{issue}} policies from the main parties in {{country}} (psst - we've hidden the party name!). Choose '{{maybe}}' to add to your shortlist, or '{{noThanks}}' to never see them again.",
  policySelectNoMaybe: {
    start:
      "Oh dear. Looks like none of those policies float your boat! It's understandable, but you still need to pick one to continue (",
    linkText: "here's why",
    linkUrl: 'faq#decide',
    end: "). Here's the list again.",
  },
  policySelectShortlist: {
    start:
      "Great! Here's your shortlist. Now select the set you prefer the most (",
    linkText: "can't decide?",
    linkUrl: 'faq#decide',
    end: ').',
  },
  results: {
    resultsIntro: "Great, you've completed the survey!",
    selectedPartiesHdr: 'Here are the parties you selected',
    resultsHdr: 'These are the results for',
    overallResultsHdr: 'These are the overall results for',
  },
  selectAll: 'Select All',
  selectAtLeastOneIssue: 'Please select at least 1 issue',
  startSurvey: 'Start the survey',
  surveySubmit: 'Submit survey',
  surveySubmitted: 'Submitting results...',
  surveyTimeEstimateTemplate:
    'With {{count}} issue selected the survey will take approx. {{from}}-{{to}} min',
  surveyTimeEstimateTemplate_plural:
    'With {{count}} issues selected the survey will take approx. {{from}}-{{to}} min',
}

export default Language
