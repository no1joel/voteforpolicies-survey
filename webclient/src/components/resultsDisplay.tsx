import React from 'react'
import { ResultsFormattedType } from '../interfaces'
import ResultsPie from './resultsPie'
import classes from './resultsDisplay.module.scss'

type Props = {
  title: string
  results: ResultsFormattedType
}

const ResultsDisplay = ({ results, title }: Props) => {
  return (
    <div className={classes.constrain}>
      <div className={classes.titleWrapper}>
        <h1 className={classes.title}>{title}</h1>
      </div>
      <div className={classes.grid}>
        <div className={classes.gridUnit}>
          <ResultsPie results={results} />
        </div>
        <div className={classes.gridUnit}>
          <div className={classes.results}>
            {results.byParty.map((p, i) => {
              const partyStyleId = p?.name.toLowerCase().replace(/ /g, '-')
              const partyClassName = `partyBlock--${partyStyleId}`
              return (
                <div
                  key={i}
                  className={`${classes.partyBlock} ${classes[partyClassName]}`}
                >
                  <dl className={classes.policyParty}>
                    <dd className={classes.hGroupMain}>
                      <span className={classes.partyBlockParty}>{p.name}</span>
                    </dd>
                    <dt className={classes.hGroupLead}>
                      {p.count === 0
                        ? 0
                        : ((p.count / results.total) * 100).toFixed(1)}
                      %
                    </dt>
                  </dl>
                </div>
              )
            })}
          </div>
        </div>
      </div>
    </div>
  )
}

export default ResultsDisplay
