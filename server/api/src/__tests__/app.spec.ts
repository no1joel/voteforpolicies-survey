import app, * as App from '../v1'
import * as Cache from '../cache/file'

import * as ApiTypes from 'vfp-survey-lib/types'

interface PostDataType {
  selections: ApiTypes.Selection[]
  filters: ApiTypes.Filter[]
}

jest.spyOn(console, 'log').mockImplementation()

const request = require('supertest')
App.init(Cache)

describe('Test the current survey endpoint', () => {
  test('Ensure we are responding type json', async () => {
    const response = await request(app).get('/survey/current')
    expect(response.statusCode).toBe(200)
    expect(response.headers['content-type']).toEqual(expect.stringContaining('json'))
  })
})

describe('Test the submit survey endpoint', () => {
  test('Test invalid postcode response if no post data', async () => {
    const response = await request(app).post('/survey/submit')
    expect(console.log).toHaveBeenLastCalledWith('Postcode filter missing from body: ', {})
    expect(response.statusCode).toBe(200)
    expect(response.headers['content-type']).toEqual(expect.stringContaining('json'))
    expect(response.text).toContain('Postcode not found')
  })

  test('Test invalid postcode response if invalid post data', async () => {
    const body: PostDataType = {
      selections: [],
      filters: [] // The server assumes one member of the array
    }
    const response = await request(app).post('/survey/submit').send(body)
    expect(console.log).toHaveBeenLastCalledWith('Postcode filter missing from body: ', body)
    expect(response.statusCode).toBe(200)
    expect(response.headers['content-type']).toEqual(expect.stringContaining('json'))
    expect(response.text).toContain('Postcode not found')
  })

  test('Test invalid postcode response if non string postcode data', async () => {
    const body = {
      selections: [],
      filters: [{
        division: 2,
        value: 0
      }]
    }
    const response = await request(app).post('/survey/submit').send(body)
    expect(response.statusCode).toBe(200)
    expect(response.headers['content-type']).toEqual(expect.stringContaining('json'))
    expect(response.text).toContain('Postcode not found')
  })

  test('Ensure we are responding type json', async () => {
    const body: PostDataType = {
      selections: [],
      filters: [{
        division: 2,
        value: 'AA'
      }]
    }

    const response = await request(app).post('/survey/submit').send(body)
    expect(response.statusCode).toBe(200)
    expect(response.headers['content-type']).toEqual(expect.stringContaining('json'))
    expect(response.text).not.toContain('Postcode not found')
  })
})
