import i18next from 'i18next'

import en from './en'

i18next.init({
  lng: 'en',

  resources: {
    en: {
      translation: en,
    },
  },

  interpolation: {
    // React already escapes values for us
    escapeValue: false,
  },
})

// https://github.com/i18next/i18next/issues/1287
const t = i18next.t.bind(i18next)

export default t
