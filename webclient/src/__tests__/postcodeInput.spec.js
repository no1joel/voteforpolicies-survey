import React from 'react'
import { render, fireEvent, screen } from '@testing-library/react'
import PostcodeInput from '../components/postcodeInput'
import t from '../translations'

describe('<PostcodeInput />', () => {
  it('does nothing if button hit and no input', () => {
    const spy = jest.fn()
    render(<PostcodeInput onComplete={spy} />)
    expect(screen.queryByText(t('surveySubmitted'))).toBeNull()
    fireEvent.click(screen.getByRole('button'))
    expect(spy).not.toHaveBeenCalled()
  })

  it('calls onComplete when button hit after input thendisplays submitted text', () => {
    const spy = jest.fn()
    render(<PostcodeInput onComplete={spy} />)
    expect(screen.queryByText(t('surveySubmitted'))).toBeNull()
    fireEvent.change(screen.getByRole('textbox'), { target: { value: 'A' } })
    fireEvent.click(screen.getByRole('button'))
    screen.getByText(t('surveySubmitted'))
    expect(spy).toHaveBeenCalled()
  })
})
