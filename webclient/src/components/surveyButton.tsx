import classnames from 'classnames'
import React from 'react'
import classes from './surveyButton.module.scss'

type Props = {
  onClick: () => void
  buttonLabel: string
  type?: 'positive' | 'negative'
  className?: string
}

const SurveyButton = ({
  buttonLabel,
  onClick,
  type = 'positive',
  className,
}: Props) => {
  return (
    <div className={className}>
      <button
        className={classnames(classes.surveyButton, classes[type])}
        onClick={onClick}
      >
        <span>{buttonLabel}</span> <i className={classes.surveyButtonIcon}></i>
      </button>
    </div>
  )
}

export default SurveyButton
