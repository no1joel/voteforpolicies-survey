import React from 'react'
import { render, screen } from '@testing-library/react'
import ResultsPie from '../components/resultsPie'

describe('<ResultsPie />', () => {
  it('renders the correct number of sectors for the number of party results provided', async () => {
    const { container } = render(
      <ResultsPie
        results={{
          total: 3,
          byParty: [
            { name: 'party 1', count: 1 },
            { name: 'party 2', count: 2 },
          ],
        }}
      />
    )

    const sectors = container.querySelectorAll('.recharts-pie-sector')

    expect(sectors.length).toBe(2)
  })

  it('renders the labels as percentages for each party result provided', async () => {
    const results = {
      total: 3,
      byParty: [
        { name: 'party 1', count: 1 },
        { name: 'party 2', count: 2 },
      ],
    }
    render(<ResultsPie results={results} />)

    const expectedParty1Pct = (
      (results.byParty[0].count / results.total) *
      100
    ).toFixed(1)
    const expectedParty2Pct = (
      (results.byParty[1].count / results.total) *
      100
    ).toFixed(1)

    screen.getByText(`${expectedParty1Pct}%`)
    screen.getByText(`${expectedParty2Pct}%`)
  })

  it('does not render percentage labels less than 5%', async () => {
    const results = {
      total: 100,
      byParty: [
        { name: 'party 1', count: 5 },
        { name: 'party 2', count: 95 },
      ],
    }
    render(<ResultsPie results={results} />)

    const expectedParty1Pct = (
      (results.byParty[0].count / results.total) *
      100
    ).toFixed(1)
    const expectedParty2Pct = (
      (results.byParty[1].count / results.total) *
      100
    ).toFixed(1)

    expect(screen.queryByText(`${expectedParty1Pct}%`)).toBeNull()
    screen.getByText(`${expectedParty2Pct}%`)
  })

  describe('renders pie sectors in the correct colour and correct label colours', () => {
    const getColours = partyName => {
      const { container } = render(
        <ResultsPie
          results={{ total: 1, byParty: [{ name: partyName, count: 1 }] }}
        />
      )
      const theOnlySector = container.querySelector('.recharts-pie-sector')
      const path = theOnlySector.querySelector('path')
      const fillColour = path.getAttribute('fill')

      const theOnlyLabel = container.querySelector('.recharts-pie-labels')
      const text = theOnlyLabel.querySelector('text')
      const labelColour = text.getAttribute('fill')
      return { fillColour, labelColour }
    }

    [
      { partyName: 'Brexit Party', expectedFillColour: '#5CBDD3', expectedLabelColour: '#000000' },
      { partyName: 'Conservatives', expectedFillColour: '#0087DC', expectedLabelColour: '#FFFFFF' },
      { partyName: 'Liberal Democrats', expectedFillColour: '#FDBB30', expectedLabelColour: '#000000' },
      { partyName: 'Labour', expectedFillColour: '#D50000', expectedLabelColour: '#FFFFFF' },
      { partyName: 'Green Party', expectedFillColour: '#75A92D', expectedLabelColour: '#FFFFFF' },
      { partyName: 'Plaid Cymru', expectedFillColour: '#3E8424', expectedLabelColour: '#FFFFFF' },
      { partyName: 'Alliance Party of Northern Ireland', expectedFillColour: '#F8CE00', expectedLabelColour: '#000000' },
      { partyName: 'Social Democratic and Labour Party', expectedFillColour: '#006E51', expectedLabelColour: '#FFFFFF' },
      { partyName: 'Sinn Féin', expectedFillColour: '#006C2A', expectedLabelColour: '#FFFFFF' },
      { partyName: 'Democratic Unionist Party', expectedFillColour: '#CE0000', expectedLabelColour: '#FFFFFF' },
      { partyName: 'Scottish National Party', expectedFillColour: '#000', expectedLabelColour: '#FFFFFF' },
      { partyName: 'Scottish Greens', expectedFillColour: '#00B543', expectedLabelColour: '#FFFFFF' }
    ].forEach(({ partyName, expectedFillColour, expectedLabelColour }) => {
      it(`colours ${partyName}`, () => {
        const { fillColour, labelColour } = getColours(partyName)
        expect(fillColour).toBe(expectedFillColour)
        expect(labelColour).toBe(expectedLabelColour)
      })
    })
  })
})
