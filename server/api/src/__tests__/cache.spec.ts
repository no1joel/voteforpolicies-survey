import * as FileCache from '../cache/file'

import fs from 'fs'
import yaml from 'js-yaml'
import toJsonSchema from '@openapi-contrib/openapi-schema-to-json-schema'

import * as ApiTypes from 'vfp-survey-lib/types'

const file = fs.readFileSync('../../lib/api.openapi.yml', 'utf8')
const doc = yaml.load(file)
const schema = toJsonSchema(doc as object)

beforeAll(() => {
  FileCache.init()
})

describe('Test the file cache store', () => {
  test('Get some survey data', async () => {
    const apiStructures = schema.components.schemas
    const data = FileCache.getSurveyData(0)

    expect(data).toHaveProperty('countries')

    const issueReq = apiStructures.Issue.required as string[]
    (data.issues as string[]).forEach(issue => {
      issueReq.forEach(property => {
        expect(issue).toHaveProperty(property)
      })
    })

    const policySetReq = apiStructures.PolicySet.required as string[]
    (data.policySets as string[]).forEach(policySet => {
      policySetReq.forEach(property => {
        expect(policySet).toHaveProperty(property)
      })
    })
  })
})

describe('Test getting survey results', () => {
  test('Get level 0 division results', async () => {
    interface ExpectedType extends ApiTypes.Result {}

    const expected: ExpectedType = {
      surveyCount: 1079640,
      results: [
        { partyId: 1, count: 942926 },
        { partyId: 2, count: 1612608 },
        { partyId: 3, count: 1678135 },
        { partyId: 4, count: 1810097 },
        { partyId: 5, count: 1656643 }
      ]
    }
    const data: ExpectedType = FileCache.getDivisionResults(0, 0, 0)
    expect(data).toEqual(expected)
  })

  test('Get level 1 division results', async () => {
    interface ExpectedType extends ApiTypes.Result {}

    const expected: ExpectedType = {
      surveyCount: 967067,
      results: [
        { partyId: 1, count: 942926 },
        { partyId: 2, count: 1612608 },
        { partyId: 3, count: 1678135 },
        { partyId: 4, count: 1810097 },
        { partyId: 5, count: 1656643 }
      ]
    }
    const data: ExpectedType = FileCache.getDivisionResults(0, 1, 1)
    expect(data).toEqual(expected)
  })

  test('Get level 2 division results', async () => {
    interface ExpectedType extends ApiTypes.Result {}

    const expected: ExpectedType = {
      surveyCount: 2064,
      results: [
        { partyId: 1, count: 2271 },
        { partyId: 2, count: 3722 },
        { partyId: 3, count: 3436 },
        { partyId: 4, count: 3661 },
        { partyId: 5, count: 3077 }
      ]
    }
    const data: ExpectedType = FileCache.getDivisionResults(0, 2, 182)
    expect(data).toEqual(expected)
  })

  test('Try to get from an invalid survey id', async () => {
    expect(() => FileCache.getDivisionResults(99, 0, 0)).toThrowError('Invalid survey id: 99')
  })

  test('Try to get invalid division', async () => {
    // toThrowError only matches the start of the error message, we want the whole string
    let caughtErr
    try {
      FileCache.getDivisionResults(0, 99, 0)
    } catch (err) {
      caughtErr = err
    }
    expect(caughtErr.message).toEqual('Invalid division level')
  })

  test('Try to get invalid level division results', async () => {
    // toThrowError only matches the start of the error message, we want the whole string
    let caughtErr
    try {
      FileCache.getDivisionResults(0, 0, 99)
    } catch (err) {
      caughtErr = err
    }
    expect(caughtErr.message).toEqual('Invalid division')
  })
})

describe('Test getting candidate data', () => {
  test('Get candidate data', async () => {
    interface ExpectedType extends ApiTypes.Party {}

    const expected: ExpectedType[] = [
      { id: 1, name: 'Brexit Party', color: '#5CBDD3' },
      { id: 2, name: 'Conservatives', color: '#0087DC' },
      { id: 3, name: 'Liberal Democrats', color: '#FDBB30' },
      { id: 4, name: 'Labour', color: '#D50000' },
      { id: 5, name: 'Green Party', color: '#75A92D' },
      { id: 6, name: 'Plaid Cymru', color: '#3E8424' }
    ]
    // TODO - Where do we filter by country?
    //        This wouldn't be the full list for the survey
    const data: ExpectedType[] = FileCache.getCandidateList(0)
    expect(data).toEqual(expected)
  })

  test('Try to get from an invalid survey id', async () => {
    expect(() => FileCache.getCandidateList(99)).toThrowError('Invalid survey id: 99')
  })
})

describe('Test getting constituency data', () => {
  test('Get constituency data', async () => {
    interface ExpectedType extends ApiTypes.Constituency {}

    const expected: ExpectedType = {
      id: 182,
      name: 'Gillingham and Rainham',
      countryId: 1
    }

    const data: ExpectedType = FileCache.getConstituencyData(0, 182)
    expect(data).toEqual(expected)
  })

  test('Try to get constituency data from an invalid survey id', async () => {
    expect(() => FileCache.getConstituencyData(99, 0)).toThrowError('Invalid survey id: 99')
  })

  test('Try to get invalid constituency id', async () => {
    expect(() => FileCache.getConstituencyData(0, 999)).toThrowError('Invalid constituency id')
  })
})

describe('Test lookup', () => {
  test('Find a value via a lookup', async () => {
    const data: number | null = FileCache.getLookup(0, 'AA')
    expect(data).toEqual(182)
  })

  test('Fail to find a value via a lookup', async () => {
    const data: number | null = FileCache.getLookup(0, 'ZZZ')
    expect(data).toBeNull()
  })

  test('Try a lookup on a non existant survey', async () => {
    expect(() => FileCache.getLookup(99, 'A')).toThrowError('Invalid survey id: 99')
  })
})
